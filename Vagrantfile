# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

if Vagrant.has_plugin?('vagrant-vbguest') then
  config.vbguest.auto_update = false
end

config.vm.provider "hyperv" do |h, override|
  h.enable_virtualization_extensions = true
  h.linked_clone = true
  h.maxmemory = 4096
  h.cpus = 2

  override.vm.network "public_network", bridge: "Default Switch"
end

config.vm.provider "vmware_desktop" do |h|
  h.gui = false
  h.vmx["memsize"] = "4096"
  h.vmx["numvcpus"] = "2"
end

config.vm.provider "libvirt" do |h|
  h.graphics_type = "none"
  h.linked_clone = true
  h.memory = 4096
  h.cpus = 2
end

config.vm.provider "virtualbox" do |h, override|
  h.gui = false
  h.memory = 4096
  h.cpus = 2
  h.customize ['modifyvm', :id, '--nested-hw-virt', 'on']

  override.vm.network "private_network", type: "dhcp"
end

config.vm.synced_folder '.', '/vagrant', disabled: true
config.vm.box_check_update = false
config.ssh.forward_agent = true
config.ssh.insert_key = false
config.ssh.extra_args = ["-o", "HostkeyAlgorithms=+ssh-rsa", "-o", "PubkeyAcceptedAlgorithms=+ssh-rsa"]

config.vm.define "kvm", primary: true do |kvm|
  kvm.vm.box = "generic/ubuntu2204"
  kvm.vm.hostname = "kvm"

  kvm.vm.network "forwarded_port", guest: 9090, host: 9090

  kvm.vm.provision "shell", inline: <<-SHELL
    apt update
    apt install -y cockpit cockpit-machines
  SHELL
end

end
